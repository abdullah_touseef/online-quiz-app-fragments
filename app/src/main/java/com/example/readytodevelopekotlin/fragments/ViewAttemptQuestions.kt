package com.example.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.readytodevelopekotlin.Adapters.adapterAttempQuestions
import com.example.readytodevelopekotlin.business.handlers.ApiHandler
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.databinding.EmptyFragmentBinding
import com.example.readytodevelopekotlin.databinding.ViewAttamptQuestionsBinding
import com.example.readytodevelopekotlin.roomDB.UserDataBase
import com.example.readytodevelopekotlin.roomDB.UserDataTable
import com.example.readytodevelopekotlin.utils.CommonMethods

class ViewAttemptQuestions : BaseFragment() {

    private lateinit var binding: ViewAttamptQuestionsBinding
    var dataList:List<UserDataTable> =ArrayList()
    companion object {
        fun newInstance(): ViewAttemptQuestions {
            return ViewAttemptQuestions()
        }
    }

    override fun initView() {
        binding.attemptRecyler.layoutManager= LinearLayoutManager(mActivity)

        val th=Thread(Runnable {
            dataList= UserDataBase?.getDatabase(mActivity)?.datauserDao()?.getData()
        })
        th.start()
        th.join()

        binding.attemptRecyler.adapter= adapterAttempQuestions(dataList,object : adapterAttempQuestions.onItemClick{
            override fun onClick()
            {
                //this is extra no use of this function in this
            }

        },mActivity)//3 parameters
    }

    override fun loadData() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ViewAttamptQuestionsBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = ViewAttemptQuestions::class.java.simpleName

}