package com.example.readytodevelopekotlin.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.DashboardBinding
import com.example.readytodevelopekotlin.databinding.ProfileBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.roomDB.UserDataBase
import com.example.readytodevelopekotlin.utils.CommonMethods

class Profile :BaseFragment(){
    lateinit var sharedPreferences: SharedPreferences
    companion object {
        fun newInstance(): Profile {
            return Profile()
        }
    }

    lateinit var binding: ProfileBinding
    override fun initView() {
        binding.backArrow.setOnClickListener {
            mActivity.supportFragmentManager.popBackStackImmediate()
        }


        binding.youIdprofile.text=PreferencesUtils(mActivity).getString("name","You")


        binding.attempIdprofile.text="Attempts "+"("+(Play.attempts.toString())+")"

        val th=Thread(Runnable {
            // set the statistics of user
            var a = UserDataBase.getDatabase(mActivity).datauserDao().getpoints()
            if(a!=null)
            {
                binding.pointsIdprofile.text=a.toString()+" Points"
                binding.PointsNo.text=a.toString()
            }

            else
            {
                binding.pointsIdprofile.text="0 Points"
                binding.PointsNo.text="0"
            }

            binding.IncorrectAnswersNo.text= UserDataBase?.getDatabase(mActivity)?.datauserDao()?.getWrongs().toString()
            binding.correctAnswersNo.text= UserDataBase?.getDatabase(mActivity)?.datauserDao()?.getRights().toString()
            binding.totalquestionsNo.text= UserDataBase?.getDatabase(mActivity).datauserDao()?.getTotalQuestion().toString()

        })
        th.start()
        th.join()




        binding.editpen.setOnClickListener {
            CommonMethods.callFragment(EditProfile.newInstance(), R.id.flFragmentContainer,
                R.anim.fade_in, R.anim.fade_out, mActivity, true)
        }
    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = Profile::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding= ProfileBinding.inflate(layoutInflater)
        return  binding.root
    }


}