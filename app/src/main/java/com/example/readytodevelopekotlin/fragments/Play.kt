package com.example.readytodevelopekotlin.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.business.handlers.ApiHandler
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.business.interfaces.RetroInterface
import com.example.readytodevelopekotlin.business.models.QuizData
import com.example.readytodevelopekotlin.business.models.UserData
import com.example.readytodevelopekotlin.databinding.EmptyFragmentBinding
import com.example.readytodevelopekotlin.databinding.PlayBinding
import com.example.readytodevelopekotlin.utils.CommonMethods
import com.example.readytodevelopekotlin.utils.CommonMethods.Companion.isNetworkAvailable
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Play : BaseFragment() {
    private lateinit var binding: PlayBinding
    //var generatedURL:String?="api.php?amount=10"
    companion object {

        var postList: List<UserData> = ArrayList()
        var cat=0
        var cat2=0
        var diff:String?=null
        var type:String?=null
        var attempts=0


        fun newInstance(): Play {
            return Play()
        }
    }

    override fun initView() {
      //  CommonMethods.setupUI(binding.root, mActivity)
        binding.backArrow.setOnClickListener {
            mActivity.supportFragmentManager.popBackStackImmediate()
        }

        var arrayAdapter= ArrayAdapter.createFromResource(
            mActivity,
            R.array.category,
            android.R.layout.simple_spinner_item
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.anyCategory.adapter=arrayAdapter
        binding.anyCategory.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position==0)
                {
                    cat=0
                    cat2=0
                }
                else
                {
                    cat=position+8
                    cat2=position
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })
        ///difficulty
        var arrayAdapter2= ArrayAdapter.createFromResource(
            mActivity,
            R.array.Difficulty,
            android.R.layout.simple_spinner_item
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.anyDifficulty.adapter=arrayAdapter2
        binding.anyDifficulty.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                //var c2=0
                if(position==0)
                {
                    diff=null
                }
                else if(position==1)
                {
                    diff="easy"
                }
                else if(position==2)
                {
                    diff="medium"
                }
                else if(position==3)
                {
                    diff="hard"
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })


        ///types
        var arrayAdapter3= ArrayAdapter.createFromResource(
            mActivity,
            R.array.TYPE,
            android.R.layout.simple_spinner_item
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.anyType.adapter=arrayAdapter3
        binding.anyType.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position==0)
                {
                    type=null
                }
                else if(position==1)
                {
                    type="multiple"
                }
                else if(position==2)
                {
                    type="boolean"
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })

    }


    override fun loadData() {
        binding.playNow.setOnClickListener {
        if (isNetworkAvailable(mActivity)) {
            binding.playNow.isEnabled = false
//            binding.animationViewloading.visibility=View.VISIBLE
            CommonMethods.showProgressDialog(mActivity)
            ApiHandler.callGetApi(10, object : OnQuizLoadListener {
                override fun onSuccess(response: String?) {
                    CommonMethods.hideProgressDialog()
                    convertIntoList(response.toString())
                    if (postList!!.isNotEmpty()) {
//                        binding.animationViewloading.visibility = View.GONE
                            PlayNow.playBackStatus=true

                        CommonMethods.callFragment(
                            PlayNow.newInstance(), R.id.flFragmentContainer,
                            R.anim.fade_in, R.anim.fade_out, mActivity, true
                        )
                        binding.playNow.isEnabled = true
                    } else {
                       // binding.animationViewloading.visibility = View.GONE

                        alerdialog3()
                        binding.playNow.isEnabled = true
                    }

                }

                override fun onError(error: String?) {
                    CommonMethods.hideProgressDialog()


//                    binding.animationViewloading.visibility = View.GONE
                    Toast.makeText(mActivity, "Something went Wrong", Toast.LENGTH_SHORT).show()
                    binding.playNow.isEnabled = true


                }
            })

        }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = PlayBinding.inflate(layoutInflater)


        return binding.root
    }



    override val fragmentTag: String
        get() = Play::class.java.simpleName







    fun alerdialog3()
    {
        var alertDialog= AlertDialog.Builder(mActivity)
        alertDialog
            .setMessage("No Question Found In your Match...!")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, which ->
                dialogInterface


            })
        alertDialog.create().show()
    }

    fun convertIntoList(response: String?) {
        val gson = Gson()
        val obj3 = gson.fromJson(response, QuizData::class.java)

        postList = obj3.results!!
        Log.e("response from api", postList.toString() )
    }

}