package com.example.readytodevelopekotlin.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.EditprofileBinding
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.utils.CommonMethods

class EditProfile:BaseFragment(){
    lateinit var binding:EditprofileBinding
//    lateinit var sharedPreferences: SharedPreferences
    companion object {
        fun newInstance(): EditProfile {
            return EditProfile()
        }
    }
    override fun initView() {

        binding.backArrow.setOnClickListener {
            mActivity.supportFragmentManager.popBackStackImmediate()
        }
        binding.youIdprofile.setText(PreferencesUtils(mActivity).getString("name",""))

        binding.updateprofile.setOnClickListener {
//            val editor=sharedPreferences.edit()

            if(binding.youIdprofile.text.isEmpty())
                PreferencesUtils(mActivity).putString("name","YOU")
            else
                PreferencesUtils(mActivity).putString("name",binding.youIdprofile.text.toString())


            mActivity!!.supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = EditProfile::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       binding= EditprofileBinding.inflate(layoutInflater)

        return binding.root
    }
}