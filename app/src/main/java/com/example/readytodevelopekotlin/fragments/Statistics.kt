package com.example.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.readytodevelopekotlin.databinding.StatisticsBinding
import com.example.readytodevelopekotlin.roomDB.UserDataBase


class Statistics:BaseFragment() {

    lateinit var binding: StatisticsBinding

    companion object {
        fun newInstance(): Statistics {
            return Statistics()
        }
    }
    override fun initView() {

        binding.backArrow.setOnClickListener {
            mActivity.supportFragmentManager.popBackStackImmediate()
        }
        binding.totalattemptsNo.text=(Play.attempts.toString())
        binding.totalquestionsNo.text=PlayNow.totalAttemptsQuestion.toString()

        val th=Thread(Runnable {
            // set the statistics of user
            var a =UserDataBase.getDatabase(mActivity).datauserDao().getpoints()
            if(a!=null)
                binding.PointsNo.text=a.toString()
            else
                binding.PointsNo.text="0"

            binding.IncorrectAnswersNo.text= UserDataBase?.getDatabase(mActivity)?.datauserDao()?.getWrongs().toString()
            binding.correctAnswersNo.text= UserDataBase?.getDatabase(mActivity)?.datauserDao()?.getRights().toString()
            binding.totalattemptsNo.text= UserDataBase?.getDatabase(mActivity).datauserDao()?.getTotalQuestion().toString()


        })
        th.start()
        th.join()
    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = Statistics::class.java.simpleName
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding= StatisticsBinding.inflate(layoutInflater)

        return  binding.root
    }

}