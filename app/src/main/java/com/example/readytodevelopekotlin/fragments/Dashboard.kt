package com.example.readytodevelopekotlin.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.DashboardBinding
import com.example.readytodevelopekotlin.fragments.Dashboard.Companion.newInstance
import com.example.readytodevelopekotlin.preferences.PreferencesUtils
import com.example.readytodevelopekotlin.roomDB.UserDataBase
import com.example.readytodevelopekotlin.utils.CommonMethods

class Dashboard:  BaseFragment() {

    lateinit var binding: DashboardBinding
//    lateinit var sharedPreferences: SharedPreferences
//    lateinit var sharedPreferences2: SharedPreferences
    companion object {
        fun newInstance(): Dashboard {
            return Dashboard()
        }
    }

    override fun initView() {


        binding.youId.text=PreferencesUtils(mActivity).getString("name","You")
        binding.quickPlayButton.setOnClickListener {
            Toast.makeText(mActivity, "Not Available This Mode", Toast.LENGTH_SHORT).show()
        }

        binding.settingButton.setOnClickListener {
            Toast.makeText(mActivity, "setting not Available", Toast.LENGTH_SHORT).show()
        }

        val th=Thread(Runnable {
            var a = UserDataBase.getDatabase(mActivity).datauserDao().getpoints()
            if(a!=null)
                binding.pointsId.text=a.toString()+" Points"
            else
                binding.pointsId.text="0 Points"


        })
        th.start()
        th.join()



        Play.attempts=(PreferencesUtils(mActivity).getInt("attp",0))
        binding.attempId.text="Attempts " + "("+(PreferencesUtils(mActivity).getInt("attp",0))+ ")"

        binding.staticButton.setOnClickListener {
            CommonMethods.callFragment(Statistics.newInstance(),R.id.flFragmentContainer,
                R.anim.fade_in, R.anim.fade_out, mActivity, true)
        }

        binding.profileButton.setOnClickListener {
            CommonMethods.callFragment(Profile.newInstance(),R.id.flFragmentContainer,
                R.anim.fade_in, R.anim.fade_out, mActivity, true)
        }

        binding.playButton.setOnClickListener {
            CommonMethods.callFragment(Play.newInstance(),R.id.flFragmentContainer,
                R.anim.fade_in, R.anim.fade_out, mActivity, true)
        }

        binding.viewAttempQuestion.setOnClickListener {
            CommonMethods.callFragment(ViewAttemptQuestions.newInstance(),R.id.flFragmentContainer,
                R.anim.fade_in, R.anim.fade_out, mActivity, true)
        }
        binding.viewPoints.setOnClickListener {
            CommonMethods.callFragment(ViewPointsByCategory.newInstance(),R.id.flFragmentContainer,
                R.anim.fade_in, R.anim.fade_out, mActivity, true)
        }
    }

    override fun loadData() {

    }

    override val fragmentTag: String?
        get() = Dashboard::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding= DashboardBinding.inflate(layoutInflater)

        return  binding.root
    }

    override fun onResume() {
        super.onResume()
        binding.youId.text=PreferencesUtils(mActivity).getString("name","You")
        Play.attempts=(PreferencesUtils(mActivity).getInt("attp",0))
        binding.attempId.text="Attempts " + "("+(PreferencesUtils(mActivity).getInt("attp",0))+ ")"
    }

}