package com.example.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.readytodevelopekotlin.Adapters.AdapterCategory
import com.example.readytodevelopekotlin.R

import com.example.readytodevelopekotlin.databinding.ViewPointsByCategoryBinding

class ViewPointsByCategory : BaseFragment() {

    var catList:ArrayList<Int>? =ArrayList()
    var pt:Int=0
    var Lines:List<String>?=null
    private lateinit var binding: ViewPointsByCategoryBinding

    companion object {
        fun newInstance(): ViewPointsByCategory {
            return ViewPointsByCategory()
        }
    }

    override fun initView() {
        Lines = resources.getStringArray(R.array.category).asList()
        binding.PointsCategoryRecyler.layoutManager= LinearLayoutManager(mActivity)
        binding.PointsCategoryRecyler.adapter= AdapterCategory(Lines!!,pt,mActivity)

    }

    override fun loadData() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ViewPointsByCategoryBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = ViewPointsByCategory::class.java.simpleName

}