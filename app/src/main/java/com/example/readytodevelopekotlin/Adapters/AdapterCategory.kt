package com.example.readytodevelopekotlin.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.readytodevelopekotlin.databinding.ItemviewPointsByCategoryBinding
import com.example.readytodevelopekotlin.roomDB.UserDataBase

class AdapterCategory ( val names: List<String>,var pt:Int,var context: Context)
    : RecyclerView.Adapter<AdapterCategory.myViewHolder>() {
    lateinit var binding: ItemviewPointsByCategoryBinding
    class myViewHolder(var binding: ItemviewPointsByCategoryBinding) : RecyclerView.ViewHolder(binding.root) {

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        binding = ItemviewPointsByCategoryBinding.inflate(inflater,parent,false)
        return myViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return names.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        holder.binding.categoryText.text= names[position]
        val th=Thread(Runnable {
            pt= UserDataBase?.getDatabase(context)?.datauserDao()?.getCatPoints(names[position])!!
            if(pt==null)
            {
                holder.binding.pointsText.text="0 Points"

            }
            else
            {
                holder.binding.pointsText.text= pt.toString()+" Points"
            }

        })
        th.start()
        th.join()

    }
}