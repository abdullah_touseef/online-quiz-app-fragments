package com.example.readytodevelopekotlin.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.databinding.ItemviewAttemptQuestionsBinding
import com.example.readytodevelopekotlin.roomDB.UserDataTable


class adapterAttempQuestions (private var listData: List<UserDataTable>, var listner: onItemClick, var context: Context)
    : RecyclerView.Adapter<adapterAttempQuestions .myViewHolder>() {
    lateinit var binding: ItemviewAttemptQuestionsBinding

    class myViewHolder(var binding: ItemviewAttemptQuestionsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onbind(mydata: UserDataTable, listner: onItemClick)
        {
            itemView.setOnClickListener {
                listner.onClick()
            }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        binding = ItemviewAttemptQuestionsBinding.inflate(inflater,parent,false)
        return myViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        val model=listData[position]
        holder.binding.questionText.text=model.question
        holder.binding.correctText.text=model.correctAnswer
        holder.binding.attemptText.text=model.yourAnswer
        //for correct or incorrect icon set in adapter
        if(model.correctAnswer.equals(model.yourAnswer))
        {
            holder.binding.tickcross.setImageResource(R.drawable.ic_baseline_check_true_24)
        }
        else
        {
            holder.binding.tickcross.setImageResource(R.drawable.ic_baseline_check_false)

        }
        holder.onbind(model,listner)

    }

    interface onItemClick {
        fun onClick()
    }


}