package com.example.readytodevelopekotlin.business.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class QuizData (
    val responseCode:Int,
    var results:List<UserData>?=null
        )



