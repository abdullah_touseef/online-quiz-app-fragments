package com.example.readytodevelopekotlin.business.interfaces

import com.example.readytodevelopekotlin.business.models.QuizData

interface OnQuizLoadListener {
    fun onSuccess(response: String?)
    fun onError(error: String?)
}