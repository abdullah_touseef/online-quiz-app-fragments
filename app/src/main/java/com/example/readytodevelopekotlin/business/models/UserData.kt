package com.example.readytodevelopekotlin.business.models

import com.google.gson.annotations.SerializedName


 class UserData (){


     var category: String? = null

     var type: String? = null

     var difficulty: String? = null

     var question: String? = null

     var correct_answer: String? = null

     var incorrect_answers:List<String>?=null

 }