package com.example.readytodevelopekotlin.business.interfaces

import com.example.readytodevelopekotlin.business.models.QuizData

interface OnServerResultNotifier {
    fun onServerResultNotifier(isError: Boolean, response: String?)
}