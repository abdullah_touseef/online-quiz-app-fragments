package com.example.readytodevelopekotlin.business.handlers

import android.view.View
import com.androidnetworking.common.Priority
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.business.interfaces.OnServerResultNotifier
import com.example.readytodevelopekotlin.business.interfaces.RetroInterface
import com.example.readytodevelopekotlin.business.models.QuizData
import com.example.readytodevelopekotlin.fragments.Play
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiHandler {

    companion object{
        var generatedUrl = getStringUrl()
        fun callGetApi(amount:Int,onQuizLoadListener:OnQuizLoadListener){
            var url:String=RetroInterface.base_url+generatedUrl
            CallForServer(url, object : OnServerResultNotifier {
                override fun onServerResultNotifier(isError: Boolean, response: String?) {

//                CommonMethods.hideProgressDialog();
                    if (!isError) {

                        onQuizLoadListener.onSuccess(response)
                    } else {
                        onQuizLoadListener.onError(response)
                    }
                }
            }).callForServerGet("quizApi",Priority.LOW)
        }


       fun getStringUrl() : String {
           var generatedURL = "api.php?amount=10"
           if(Play.cat!=0)
           {
               generatedURL+="&category="+ Play.cat
           }
           if(Play.diff!=null)
           {
               generatedURL+="&difficulty="+ Play.diff
           }
           if(Play.type!=null)
           {
               generatedURL+="&type="+ Play.type
           }

           return generatedURL
       }


    }

}