package com.example.readytodevelopekotlin.roomDB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.room.mvvm.room.LoginDatabase


@Database(entities = [UserDataTable::class],version=1)
abstract class UserDataBase: RoomDatabase(){
    abstract fun datauserDao():UserDataDao
    companion object {
        @Volatile
        private var INSTANCE: UserDataBase? = null

//
//        @Volatile
//        private var INSTANCE: LoginDatabase? = null

//        fun getDataseClient(context: Context) : UserDataBase {
//
//            if (INSTANCE != null) return INSTANCE!!
//
//            synchronized(this) {
//
//                INSTANCE = Room
//                    .databaseBuilder(context, UserDataBase::class.java, "DATABASE")
//                    .fallbackToDestructiveMigration()
//                    .build()
//
//                return INSTANCE!!
//
//            }
//        }

        fun getDatabase(context: Context): UserDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, UserDataBase::class.java, "mydatabase"
            ).build()
    }
}